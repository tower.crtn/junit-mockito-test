package com.ids.test.junit.mockito.junit.mockito.test.controller;

import com.ids.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.ids.test.junit.mockito.junit.mockito.test.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("${properties.api.uri.basePath}")
public class TestController {

  @Autowired
  private PersonService personService;

  @GetMapping(value = "${properties.api.uri.specificPaths.getTest}")
  public ResponseEntity<List<PersonModel>> getAllPersons(){
    List<PersonModel> persons = personService.getAllPersonIdSeparateForId();
    
    HttpStatus httpStatus = HttpStatus.NOT_FOUND;
    ResponseEntity<List<PersonModel>> responseEntityPersons = null;
    if(!persons.isEmpty()) {
      httpStatus = HttpStatus.OK;
      responseEntityPersons = new ResponseEntity<>(persons, httpStatus);
    } else {
      responseEntityPersons = new ResponseEntity<>(httpStatus);
    }
    return responseEntityPersons;
  }
  
  @PostMapping(value = "${properties.api.uri.specificPaths.postTest}")
  public ResponseEntity<PersonModel> getOnePerson(PersonModel personModelRequest) {

    PersonModel personModel = personService.getPersonById(personModelRequest);

    HttpStatus httpStatus = HttpStatus.NOT_FOUND;
    ResponseEntity<PersonModel> responseEntity = null;
    if (!Objects.isNull(personModel)) {
      httpStatus = HttpStatus.OK;
      responseEntity = new ResponseEntity<>(personModel, httpStatus);
    } else {
      responseEntity = new ResponseEntity<>(null, httpStatus);
    }

    return responseEntity;
  }

}
