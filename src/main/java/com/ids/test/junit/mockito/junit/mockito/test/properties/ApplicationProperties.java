package com.ids.test.junit.mockito.junit.mockito.test.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@Getter
public class ApplicationProperties {
  
  @Value("${properties.statusReason.defaul}")
  private String defaultReason;
  
  @Value("${properties.statusReason.00}")
  private String postulanteReason;
  
  @Value("${properties.statusReason.01}")
  private String inscritoReason;
  
  @Value("${properties.statusReason.02}")
  private String enCursoReason;
  
  @Value("${properties.statusReason.03}")
  private String bajaTemporalReason;
  
  @Value("${properties.statusReason.04}")
  private String bajaDefinitivaReason;
  
}
