package com.ids.test.junit.mockito.junit.mockito.test.service;

import com.ids.test.junit.mockito.junit.mockito.test.model.PersonModel;
import java.util.List;

public interface PersonService {
  
  public PersonModel getPersonById(PersonModel personModel);
  
  public List<PersonModel> getAllPersonIdSeparateForId();
}
