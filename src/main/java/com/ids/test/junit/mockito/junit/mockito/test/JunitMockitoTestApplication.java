package com.ids.test.junit.mockito.junit.mockito.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitMockitoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunitMockitoTestApplication.class);
	}

}
