package com.ids.test.junit.mockito.junit.mockito.test.entity;

import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity
@IdClass(PersonEntityKey.class)
public class PersonEntity {
  
  @Id
  @Column(name = "PERSON_ID")
  private int personId;
  
  @Column(name = "NAME")
  private String name;
  
  @Column(name = "LAST_NAME")
  private String lastName;
  
  @Column(name = "BIRTH_DATE")
  private String birthDate;
  
  @Column(name = "STATUS")
  private String status;
}
