package com.ids.test.junit.mockito.junit.mockito.test.repository;

import com.ids.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.ids.test.junit.mockito.junit.mockito.test.entity.PersonEntityKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, PersonEntityKey>{
  
  PersonEntity findByPersonId(@Param("PERSON_ID") int personId);
  
}
